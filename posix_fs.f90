!-------------------------------------------------------------------------------
! $Id$
!-------------------------------------------------------------------------------
! FS_MKDIR, FS_RENAME, FS_UNLINK, FS_REMOVE are Fortran bindings to the
! standard C POSIX filesystem functions.
! Copied from the CSV_IO modue of *HEDTOOLS*
! (https://tegsvn.uib.no/svn/tegsvn/tags/HEDTOOLS/1.1)
!-------------------------------------------------------------------------------
module m_fs_posix

  ! Global constants.
  ! used only within the module
  integer, parameter, private :: MAX_UNIT=255      ! Maximum unit number

  ! GET_FREE_FUNIT from HEDTOOLS is only for internal use. It is pasted here
  ! to avoid interdependence between modules.
  private :: GET_FREE_FUNIT

  contains

  !-----------------------------------------------------------------------------
  ! # GET_FREE_FUNIT #
  ! Purpose: returns the first free Fortran unit number (search from 1 to
  ! MAX_UNIT).
  ! ## RETURNS: ##
  !  - Integer unit number
  ! ## CALL PARAMETERS ##
  !  - optional logical execution error status (.TRUE.)
  !  - optional integer max_funit to search (default MAX_UNIT defined in
  !    module)
  !
  ! Author: John Burkardt : This code is distributed under the GNU LGPL license.
  ! Modified by Sergey Budaev.
  !
  ! This subroutine is a part of the *HEDTOOLS* suite. The stable SVN trunk
  ! address is here: https://tegsvn.uib.no/svn/tegsvn/tags/HEDTOOLS/1.1
  !-----------------------------------------------------------------------------
  function GET_FREE_FUNIT (file_status, max_funit) result (file_unit)

    use, intrinsic :: ISO_FORTRAN_ENV     !Provides system-wide scalar constants
                                          !INPUT_UNIT OUTPUT_UNIT ERROR_UNIT
    implicit none

    !Function value
    integer :: file_unit

    !Calling parameters
    logical, optional, intent(out) :: file_status
    integer, optional, intent(in)  :: max_funit

    !Local variables: copies of optional parameters we need co copy optional
    logical  :: file_status_here  ! Variables in case they are absent, so always
    integer  :: max_funit_here    ! Work with copies of optionals inside
    !Other local variables
    integer :: i
    integer :: ios
    logical :: lopen

    !Subroutine name for DEBUG LOGGER
    character (len=*), parameter :: PROCNAME = "GET_FREE_FUNIT"

    file_unit = 0
    file_status_here = .FALSE.

    if (present(max_funit)) then
        max_funit_here=max_funit
      else
        max_funit_here=MAX_UNIT !Max from globals
    end if

    do i=1, max_funit_here
      if (i /= INPUT_UNIT .and. i /= OUTPUT_UNIT .and. &
            i /= ERROR_UNIT) then             !Exclude standard console units
        inquire (unit=i, opened=lopen, iostat=ios)
        if (ios == 0) then
          if (.not. lopen) then
            file_unit = i                     !First free unit found
            file_status_here=.TRUE.
            if (present(file_status)) file_status=file_status_here
            return
          end if
        end if
      end if
    end do

    if (.not. file_status_here) file_unit=-1    !If no free unit found return -1
    if (present(file_status)) file_status=file_status_here ! and error flag

  end function GET_FREE_FUNIT

  subroutine FS_MKDIR(dirname, iostat, is_writeable)
  !=============================================================================
  ! FS_MKDIR
  ! PURPOSE: Makes a durectory using a POSIX standard C call via the Fortran
  !          interface.
  ! See C file system interface docs (GNU C):
  !https://www.gnu.org/software/libc/manual/html_node/File-System-Interface.html
  !
  ! CALL PARAMETERS:
  !    Character directory name.
  !    Optional iostat integer parameter reports the success, however, Fortran
  !          may not support POSIX bindings, so the output may have no use
  !          (have to check!). Works on gfortran, ifort, f95 (Oracle)
  !             0 = success (directory created);
  !            -1 = there was an error creating directory. Note that this error
  !                 is reported even if the directory with the same name already
  !                 exists. So error reporting might not be fully useful.
  !
  ! Author: Sergey Budaev
  !=============================================================================

  use ISO_C_BINDING

    character(len=*), intent(in) :: dirname
    integer, optional, intent(out) :: iostat
    logical, optional, intent(out) :: is_writeable

    integer :: cret

    character(len=*), parameter :: f_name_tmp = ".tmpfile"
    integer :: f_unit_tmp, iostat_f_tmp

    interface
      function mkdir(path,mode) bind(c,name="mkdir")
        use ISO_C_BINDING
        integer(c_int) :: mkdir
        character(kind=c_char,len=1) :: path(*)
        integer(c_int16_t), value :: mode
      end function mkdir
    end interface

    ! Call C through interface/binding. Requires F2003 to work.
    ! Note: o'755' is the default octal permissions on the directory, if
    !       supported by the system.
    cret = mkdir (dirname // char(0), int(o'755',c_int16_t) )

    ! This return parameter might not actually work on all systems.
    if (present(iostat)) iostat = cret

    ! Try to make sure the directory is writeable, if is_writeable optional
    ! parameter was requested. Open a small tmp file for writing and check
    ! the operation status.
    CHECK_WRITE_DIR: if (present(is_writeable)) then
      f_unit_tmp = GET_FREE_FUNIT()
      open( unit=f_unit_tmp, file=trim(dirname) // f_name_tmp,                &
            status='replace', iostat=iostat_f_tmp )
      if (iostat_f_tmp == 0) then
        is_writeable = .TRUE.
      else
        is_writeable = .FALSE.
      end if
      close(f_unit_tmp)
      call FS_UNLINK( trim(dirname) // f_name_tmp )
    end if CHECK_WRITE_DIR

  end subroutine FS_MKDIR

  !-----------------------------------------------------------------------------

  subroutine FS_RENAME(dir_from, dir_to, iostat)
  !=============================================================================
  ! FS_RENAME
  ! PURPOSE: Renames a file or directory using a POSIX standard C call via the
  !          Fortran interface.
  !
  ! See C file system interface docs (GNU C):
  !https://www.gnu.org/software/libc/manual/html_node/File-System-Interface.html
  !
  ! CALL PARAMETERS:
  !    Character existing file/directory name.
  !    Character rename to name.
  !    Optional iostat integer parameter reports the success, however, Fortran
  !          may not support POSIX bindings, so the output may have no use
  !          (have to check!). Works on gfortran, ifort, f95 (Oracle)
  !             0 = success (directory created);
  !            -1 = there was an error.
  !
  ! Author: Sergey Budaev
  !=============================================================================

  use ISO_C_BINDING

    character(len=*), intent(in) :: dir_from
    character(len=*), intent(in) :: dir_to
    integer, optional, intent(out) :: iostat

    integer :: cret

    interface
      function rename(frompath,topath) bind(c,name="rename")
        use iso_c_binding
        integer(c_int) :: rename
        character(kind=c_char,len=1) :: frompath(*)
        character(kind=c_char,len=1) :: topath(*)
      end function rename
    end interface

    ! Call C through interface/binding. Requires F2003 to work.
    cret = rename (dir_from // char(0), dir_to // char(0) )

    ! This return parameter might not actually work on all systems.
    if (present(iostat)) iostat = cret

  end subroutine FS_RENAME

  !-----------------------------------------------------------------------------

  subroutine FS_UNLINK(filename, iostat)
  !=============================================================================
  ! FS_UNLINK
  ! PURPOSE: Deletes a file (NOT a directory) using a POSIX standard C call via
  !          the Fortran interface. Note that this subroutine does not delete
  !          directories for safety. Use FS_REMOVE for this.
  !
  ! See C file system interface docs (GNU C):
  ! https://www.gnu.org/software/libc/manual/html_node/File-System-Interface.html
  !
  ! CALL PARAMETERS:
  !    Character existing file name to be deleted.
  !    Optional iostat integer parameter reports the success, however, Fortran
  !          may not support POSIX bindings, so the output may have no use
  !          (have to check!). Works on gfortran, ifort, f95 (Oracle)
  !             0 = success (directory created);
  !            -1 = there was an error.
  !
  ! Author: Sergey Budaev
  !=============================================================================

  use ISO_C_BINDING

    character(len=*), intent(in) :: filename
    integer, optional, intent(out) :: iostat

    integer :: cret

    interface
      function unlink(filename) bind(c,name="unlink")
        use iso_c_binding
        integer(c_int) :: unlink
        character(kind=c_char,len=1) :: filename(*)
      end function unlink
    end interface

    ! Call C through interface/binding. Requires F2003 to work.
    cret = unlink (filename // char(0) )

    ! This return parameter might not actually work on all systems.
    if (present(iostat)) iostat = cret

  end subroutine FS_UNLINK

  !-----------------------------------------------------------------------------

  subroutine FS_REMOVE(filename, iostat)
  !=============================================================================
  ! FS_REMOVE
  ! PURPOSE: Deletes a file or a directory using a POSIX standard C call via
  !          the Fortran interface.
  !
  ! See C file system interface docs (GNU C):
  !https://www.gnu.org/software/libc/manual/html_node/File-System-Interface.html
  !
  ! CALL PARAMETERS:
  !    Character existing file name to be deleted.
  !    Optional iostat integer parameter reports the success, however, Fortran
  !          may not support POSIX bindings, so the output may have no use
  !          (have to check!). Works on gfortran, ifort, f95 (Oracle)
  !             0 = success (directory created);
  !            -1 = there was an error.
  !
  ! Author: Sergey Budaev
  !=============================================================================

  use ISO_C_BINDING

    character(len=*), intent(in) :: filename
    integer, optional, intent(out) :: iostat

    integer :: cret

    interface
      function remove(filename) bind(c,name="remove")
        use iso_c_binding
        integer(c_int) :: remove
        character(kind=c_char,len=1) :: filename(*)
      end function remove
    end interface

    ! Call C through interface/binding. Requires F2003 to work.
    cret = remove (filename // char(0) )

    ! This return parameter might not actually work on all systems.
    if (present(iostat)) iostat = cret

  end subroutine FS_REMOVE

end module m_fs_posix
