# Binary_IO

Fortran module to input and output multidimensional arrays.

**Author:** Camilla Håkonsrud Jensen

## Changelog

- Imported code from Camilla's hormone model, no changes.

## About

Binary_IO is a module written to write arrays to binary files (array_to_binary)
and read binary files into arrays (binary_to_array). The module also
contains diagnostic tools for use with binary files, as well as a function
for returning the first free Fortran unit number.

## How to use

To use Binary_IO add this line in the top of your code:

    use binary_IO

This line should be added before `implicit none` and parameter and variable
declarations.

### Write array to binary file

Then write this line when

    call array_to_binary(<array>, <filename>, <optional unit number>, <optional error status>)

Named optional parameters: **integer** `funit` is unit number, **integer**
`error` is the error status.

But usually it will be enough with:

    call array_to_binary(<array>, <filename>)

### Read array from binary file

To read an array from a binary add "**use binary_IO**" to the top of your
code before **implicit none**. Then add this line where you want to read an
array from a binary file.

    call binary_to_array(<array>, <filename>, <optional unit number>, <optional error status>)

Named optional parameters: **integer** `funit` is unit number, **integer**
`error` is the error status.

But usually it will be enough with:

    call binary_to_array(<array>, <filename>)

## How to import binary data

### R

To import binary files of the format produced by Binary_IO you will have to use
two different functions depending on the array type is contained in the file.

For arrays containing real values add this function to the top of your R script:

    #Function of importing binary files containing real values
    # following http://www.ats.ucla.edu/stat/r/faq/read_binary.htm
    import.binary <- function(file_path) {
      bin <- file(file_path, "rb")
      ranking <- readBin(bin,n=1,integer())
      dim_length <- readBin(bin, n = ranking, integer())
      bin.data <- readBin(bin, n = prod(dim_length), numeric())
      bin.data <- array(bin.data,dim=dim_length)
    }

To use in your script write:

    dataset <- import.binary("<filename>")

If you, on the other hand, want to import a binary file containing an array
with integer values you will have to add this function to the top of your
R script:

    #Function of importing binary files containing integer values
    # following http://www.ats.ucla.edu/stat/r/faq/read_binary.htm
    import.binary.int <- function(file_path) {
      bin <- file(file_path, "rb")
      ranking <- readBin(bin,n=1,integer())
      dim_length <- readBin(bin, n = ranking, integer())
      bin.data <- readBin(bin, n = prod(dim_length), integer())
      bin.data <- array(bin.data,dim=dim_length)

And to use simply write:

    dataset <- import.binary.int("<filename>")

For further data analysis it is also recommend to use the melt function in the [https://cran.r-project.org/web/packages/reshape2/reshape2.pdf](reshape2 package), for example:

    library(reshape2)
    melt(dataset)

This will turn your multidimensional array into a two dimensional dataset, which is easier to work with in R. This 2D dataset can then further be manipulated in R using for example the [https://cran.r-project.org/web/packages/tidyverse/tidyverse.pdf](tidyverse package).

### MATLAB

To import the Binary_IO binary files to MATLAB, you will also need to use
two different pieces of code like in R.

For importing binary files containing arrays with double-precision real
values use:

    filename = fopen([<inputfolder>, '<filename>']);
    rank = fread(filename,1,'int');
    dim_length(1:rank) = fread(filename,rank,'int');
    array = fread(filename,'double');
    array = reshape(array,dim_length(1:rank));
    fclose(filename);

For importing binary files containing arrays with integer values use:

    filename = fopen([<inputfolder>, '<filename>']);
    rank = fread(filename,1,'int');
    dim_length(1:rank) = fread(filename,rank,'int');
    array = fread(filename,'int');
    array = reshape(array,dim_length(1:rank));
    fclose(filename);
