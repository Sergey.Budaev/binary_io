!-------------------------------------------------------------------------------
! $Id$
!-------------------------------------------------------------------------------
!> Module implementing various general procedures for operating real type
!! variables and numbers:
!!  - Define the precision kind constants and tolerance limits:
!!  - Define functions for checking near-equality of real numbers.
module m_realcalc

  !> These constants define single, double and quadruple precision kinds
  !! in a portable way, specified by (a) minimum decimal precision and/or
  !! (b) exponent range. **Use this as a template.**
  integer, parameter, private :: S_PREC_32  = selected_real_kind( 6,   37)
  integer, parameter, private :: D_PREC_64  = selected_real_kind(15,  307)
  integer, parameter, private :: Q_PREC_128 = selected_real_kind(33, 4931)

  !> Standard precision parameter. This parameter defines the real type
  !! kind for use in all other parts of the code.
  !!  - **SRP** stands for Standard Real Precision
  integer, parameter, public :: SRP = D_PREC_64

  !> This is just zero, its use makes it easier to avoid caveats and errors
  !! with literal constants, e.g. placing 0.0 (that assumes the default kind)
  !! in a function that accepts real(SRP) argument:
  !! func(0.0) might result in error while func(NIL) is always correct.
  real(SRP), parameter, public :: NIL = 0.0_SRP

  !> Missing data code. A real type code that is placed if a value is missing
  !! for some reason. Using a weird number like -9999.0 makes it easier to
  !! detect wrong data usage because this large negative value is likely to
  !! propagate in calculations and be easily detectable. All numbers should
  !! be ideally initialised to m_realcalc::missing.
  real(SRP), parameter, public :: MISSING = -9999.0_SRP

  !> Tolerance parameter. It is defined as  **twice** the smallest `real`
  !! number *E* such that @f$ 1 + E > 1 @f$.
  real(SRP), parameter, public :: SMALL = epsilon(0.0_SRP) * 2.0_SRP

  !> Float equal operator. tests if two real values are equal with some small
  !! tolerance equal to m_realcalc::small.
  !!
  !! Usage:
  !! @code
  !!   if ( a .feq. b ) ...
  !! @endcode
  !! is roughly equivalent to
  !! @code
  !!   if ( a == b ) ...
  !! @endcode
  interface operator (.feq.)
    procedure float_equal_op_feq
  end interface operator (.feq.)

  private float_equal_op_feq

contains

  !> Check if two real values are nearly equal using the
  !! Thus function can be used for comparing two real values like the below.
  !! The exact comparison (incorrect due to possible rounding):
  !! @code
  !!   if ( a == b ) ...
  !! @endcode
  !! should be substituted by such comparison:
  !! @code
  !!   if ( float_equal(a, b, 0.00001) ) ...
  !! @endcode
  elemental function float_equal(value1, value2, epsilon) result (equal)
    !> The first value for comparison.
    real(SRP), intent(in) :: value1
    !> The second value for comparison.
    real(SRP), intent(in) :: value2
    !> optional (very small) tolerance value.
    real(SRP), intent (in), optional :: epsilon
    !> TRUE if the values `value1` and `value2` are nearly equal.
    logical :: equal

    real(kind(epsilon)) :: local_epsilon
    local_epsilon = SMALL

    if ( present(epsilon) ) then
      local_epsilon = abs(epsilon)
    end if

    equal = abs( value1-value2 ) <= local_epsilon

  end function float_equal

  !> A simplified version of m_realcalc::float_equal() for use in the `.feq.`
  !! operator.
  elemental function float_equal_op_feq(value1, value2) result (equal)
    !> The first value for comparison.
    real(SRP), intent(in) :: value1
    !> The second value for comparison.
    real(SRP), intent(in) :: value2
    !> TRUE if the values `value1` and `value2` are nearly equal.
    logical :: equal

    equal = abs( value1-value2 ) <= SMALL

  end function float_equal_op_feq

  !> Calculate an average of a one-dimensional array X.
  !! Example use:
  !! @code
  !! mean = average(X)                ! default missing code is used
  !! mean2 = average(X, -99999.0_SRP) ! missing data code supplied
  !! @endcode
  pure function average(X, missing_code) result (A)
    !> input 1D array.
    real(SRP), dimension(:), intent(in) :: X
    !> optional code for missing data that are ignored in calculations.
    real(SRP), intent(in), optional :: missing_code
    !> returned average value
    real(SRP) :: A

    ! Local copy used for missing value, we cannot easily use the argument
    ! itself because it can not be provided, in this case the variable just
    ! does not exist.
    real(SRP) :: missing_loc

    ! This checks if the missing data argument was passed into the function
    ! and if no, use the global missing data defined in m_realcalc::missing
    if (present(missing_code)) then
      missing_loc = missing_code
    else
      missing_loc = MISSING
    end if

    A = sum(X, X/= missing_loc) / count (X /= missing_loc)

  end function average


  !> Rescale input value X from 0:1 to arbitrary range A:B.
  !! Example call:
  !! @code
  !! X = rescale10( X, rmin, rmax )
  !! X = rescale10( X, 10.0_SRP, 1000.0_SRP )
  !! @endcode
  elemental function rescale10(X, A, B) result (RX)
    !> Input value within 0:1
    real(SRP), intent(in)  :: X
    !> Rescale range, lower
    real(SRP), intent(in)  :: A
    !> Rescale range, upper
    real(SRP), intent(in)  :: B
    real(SRP) :: RX

    RX = X * (B-A) + A

  end function rescale10

end module m_realcalc
