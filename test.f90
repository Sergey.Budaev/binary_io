! Here is a short test program that illustrates how to
! use the binary read procedures:
!
! ifort -o zzz m_realcalc.f90 binary_IO.f90 posix_fs.f90  test.f90
!
program test

    use m_realcalc, only : RP => SRP, MISSING
    use binary_IO, FOLDER => FIX_FOLDER
    use m_fs_posix

    integer, dimension(5,15,25,35,10) :: ZZ = -9999
    integer, allocatable, dimension(:,:,:,:,:) :: aZZ
    integer, dimension(5,15,25,35,10) :: fZZ

    real(RP), dimension(5,15,25,35,10) :: XX = MISSING
    real(RP), dimension(5,15,25,35,10) :: fXX

    real(RP), dimension(50,50) :: YY
    real(RP), dimension(50,50) :: fYY

    integer, allocatable, dimension(:) :: dims
    integer :: ier
    logical :: can_write

    ! Make folder for data (FIX_FOLDER) - do not forget to use trim(FIX_FOLDER)
    ! function because FIX_FOLDER is fixed length string.
    FOLDER = "./xxx/"
    print *, "Trying to create """, trim(FOLDER), """ folder."
    call FS_MKDIR( trim(FOLDER), is_writeable=can_write )
    ! Check if the newly created folder can actually be used for writing
    ! Note that overwriting existing folder is ignored (but iostat indicator
    ! can be used to check, see FS_MKDIR code).
    if ( can_write .eqv. .FALSE. ) then
      print *, "ERROR: The folder """, trim(FOLDER), """ is NOT writeable!"
      stop 255
    else
      print *, "Folder """, trim(FOLDER), """ is writeable"
    end if

    !---------------------------------------------------------------------------
    ! Integer data (ZZ)
    print *, "" ;  print *, "** Checking integer array ZZ"

    ! Write data to a binary file.
    call array_to_binary(ZZ, "zzz.bin")
    print *, "Array size: ", size(ZZ)

    ! Use the diagnostic functions to check what the files are like.
    print *, "RANK = ", binary_get_rank("zzz.bin")
    print *, "DIMS = ", binary_get_dims("zzz.bin")

    ! Read data (fixed array):
    call binary_to_array(fZZ, "zzz.bin", error=ier)
    print *, "Array size: ", size(fZZ), ", error: ", ier
    print *, "  Check:: Any input unequal to output (error): ", any(ZZ /= fZZ)

    ! Read data (allocatable array):
    ! - get the dimensions
    dims = binary_get_dims("zzz.bin")
    ! - allocate the aray for data input
    allocate( aZZ(dims(1), dims(2), dims(3), dims(4), dims(5)) )
    ! - read data
    call binary_to_array(aZZ, "zzz.bin", error=ier)
    ! - print diagnostics
    print *, "Array size: ", size(aZZ), ", error: ", ier
    print *, "  Check:: Any input unequal to output (error): ", any(ZZ /= aZZ)

    !---------------------------------------------------------------------------
    ! Real data (XX)
    print *, "" ; print *, "** Checking real array XX"

    ! Write data to a binary file.
    call array_to_binary(XX, "zzz.bin")
    print *, "Array size: ", size(XX)

    ! Use the diagnostic functions to check what the files are like.
    print *, "RANK = ", binary_get_rank("zzz.bin")
    print *, "DIMS = ", binary_get_dims("zzz.bin")

    ! Read data (fixed array):
    call binary_to_array(fXX, "zzz.bin", error=ier)
    print *, "Array size: ", size(fXX), ", error: ", ier
    print *, "  Check:: Any input unequal to output (error): ", any(XX /= fXX)

    !---------------------------------------------------------------------------
    ! Real data (random data YY)
    print *, "" ; print *, "** Checking real array YY"

    call random_number(YY)

    ! Write data to a binary file.
    call array_to_binary(YY, "zzz.bin")
    print *, "Array size: ", size(YY)

    ! Use the diagnostic functions to check what the files are like.
    print *, "RANK = ", binary_get_rank("zzz.bin")
    print *, "DIMS = ", binary_get_dims("zzz.bin")

    ! Read data (fixed array):
    call binary_to_array(fYY, "zzz.bin", error=ier)
    print *, "Array size: ", size(fYY), ", error: ", ier
    print *, "  Check:: Any input unequal to output (error): ", any(YY /= fYY)

    !---------------------------------------------------------------------------
    ! Check error when the file and input arrays have different ranks
    print *, "" ; print *, "** Checking rank mismatch error"

    call array_to_binary(ZZ, "zzz.bin") ! write rank 5
    print *, "Write data array rank: ", rank(ZZ)
    call binary_to_array(fYY, "zzz.bin", error=ier) ! read rank 2
    print *, "Read data array rank: ", rank(fYY)
    print *, "Error code returned: ", ier

end program test
